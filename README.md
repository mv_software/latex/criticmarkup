# criticmarkup

## Overview

A LaTeX package to annotate documents using an embellished version of the Critic Markup syntax (http://criticmarkup.com/users-guide.php).

It is not easy to annotate in LaTeX (not in Markdown). Although one can annotate the PDF output, the number of mouse clicks is bothersome. Critic Markup format offers a happy medium, but there doesn't appear to be a simple package to do that in LaTeX.

## Installation

To install the package, simply place the STY file in the same folder as the LaTeX file that you are compiling. If you want it globally, install it to the local texmf folder. You can find that folder using the command `kpsewhich -var-value=TEXMFHOME`. On a Mac, it is `~/Library/texmf`. I recommend placing the STY file under a subfolder. For example, `~/Library/texmf/tex/latex/custom/`.

## Usage

### Preamble

\usepackage[name=ShortName]{criticmarkup}

### Commands
``` latex
\com{This is a comment.}
\com[SE]{Someone else has left a comment.}
\add{This is new text.}
\add[SE]{This is new text added by someone else.}
\del{This is deleted text.}
\del[SE]{Someone else deleted this text.}
The \sub{old text}{is replaced with new text}.
The \sub[SE]{old text}{is replaced with new text by someone else}.
\hil{I have something to say.}{This is not a good idea!}
\hil[SE]{I have something to say.}{Someone else thinks that this is not a good idea!}
```